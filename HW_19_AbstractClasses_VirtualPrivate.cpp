#include <iostream>

class Animal {
public:
    static int AnimalCounter;

    virtual void Voice() {
        std::cout << "\0" << std::endl;
    }

    Animal() {
        ++AnimalCounter;
    }

    int static GetCounter() {
        return AnimalCounter;
    }
};

class Cat : public Animal {
public:
    void Voice() override {
        std::cout << "Meow" << std::endl;
    }
};

class Dog : public Animal {
public:
    void Voice() override {
        std::cout << "Woof!" << std::endl;
    }
};

class Sheep : public Animal {
public:
    void Voice() override {
        std::cout << "Beee" << std::endl;
    }
};

int Animal::AnimalCounter = 0;

int main() {
    Cat* CatPtr = new Cat;
    Sheep* SheepPtr = new Sheep;
    Dog* DogPtr = new Dog;

    //Create array with class objects
    Animal** ArrayOfVoices = new Animal*[Animal::GetCounter()];
    ArrayOfVoices[0] = CatPtr;
    ArrayOfVoices[1] = SheepPtr;
    ArrayOfVoices[2] = DogPtr;

    for (int i = 0; i < Animal::GetCounter(); ++i) {
        if (ArrayOfVoices[i] != nullptr) {
            ArrayOfVoices[i]->Voice();
            delete ArrayOfVoices[i];
        }
        else {
            std::cout << "Error with pointers!" << std::endl;
            continue;
        }
    }

    delete[] ArrayOfVoices;

    return 0;
}


